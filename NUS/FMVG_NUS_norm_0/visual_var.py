import numpy as np 
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
from scipy.stats import norm

def G(miu):
	sqrt2=np.sqrt(2)
	l=0
	r=10
	while (abs(l-r)>1e-4):
		mid1=l+(r-l)/3.0
		mid2=mid1+(r-l)/3.0
		f1=norm.cdf(sqrt2*mid1+miu)/norm.cdf(mid1+miu)	
		f2=norm.cdf(sqrt2*mid2+miu)/norm.cdf(mid2+miu)	
		if (f1<f2):
			l=mid1
		else:
			r=mid2
	return norm.cdf(sqrt2*l+miu)/norm.cdf(l+miu)
	
sigma=np.load("sigma.npy")
W,U=np.linalg.eig(sigma)
fac1=np.sqrt(np.linalg.det(2*sigma+np.eye(sigma.shape[0])))
print "|2Sigma+I|^{1/2}=", fac1

MIUP=np.load("MIUP.npy")
idx=np.arange(MIUP.shape[0])
np.random.shuffle(idx)
L=[]
m=100
n=81
for j in range(m):
	g=1
	for i in range(n):
		g*=G(MIUP[idx[j]][i])
	print g
	L.append(g)
L.sort()
print L
avg=L[int(m/2)]
print avg
print fac1*avg, np.log(fac1*avg)

