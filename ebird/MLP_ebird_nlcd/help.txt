config.py:  records all constants, hyperparameters and paths.
train.py: run FMVG to train the model
test.py: run FMVG to test the performance on testing datasets
test_margin_rnll: not finished yet (It is designed to output margin distribution)
analysis.py: used for analysi my algorithm
FMVG.py: provides FMVG model class
FMVG_test.py: provides FMVG model class as well as some prediction functions and analysis
FMVG_indiv.py: provides variant of FMVG for individual distribution.
get_data: provides functions to read batches from whole datasets for training, validation or testing.
runTrain.sh: shell to "rm -r model; rm -r summary; python train.py"
TB.sh: run  tensorboard
*.pyc: generated files while running .py files.

