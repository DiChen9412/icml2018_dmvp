import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import FMVG_test as FMVG
import Predictor
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS
def initialize_uninitialized(sess):
    global_vars          = tf.global_variables()
    is_not_initialized   = sess.run([tf.is_variable_initialized(var) for var in global_vars])
    not_initialized_vars = [v for (v, f) in zip(global_vars, is_not_initialized) if not f]

    #print [str(i.name) for i in not_initialized_vars] # only for testing
    if len(not_initialized_vars):
        sess.run(tf.variables_initializer(not_initialized_vars))

def main(_):

	print 'reading npy...'

	#images = np.load(FLAGS.image_dir)
	data = np.load(FLAGS.data_dir)
	test_idx = np.load(FLAGS.test_idx)

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print 'building network...'

	classifier = FMVG.FMVG(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print 'restoring from '+FLAGS.checkpoint_path


	def test_step():
		print 'Testing...'
		all_nll_loss_opt = 0
		all_nll_loss_marginal = 0
		all_nll_loss_appro = 0
		error_rate1=0
		error_rate2=0
		wa=0
		real_batch_size=min(FLAGS.testing_size, len(test_idx))
		run_cnt=0
		
		TP=0
		TN=0
		FN=0
		FP=0
		for k in range(int( (len(test_idx)-1)/real_batch_size )+1):
			
			start = real_batch_size*k
			end = min(real_batch_size*(k+1), len(test_idx))
			run_cnt+=end-start

			#input_image= get_data.get_image(images, test_idx[start:end])
			input_nlcd = get_data.get_nlcd(data,test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])

			feed_dict={}
			feed_dict[classifier.input_nlcd]=input_nlcd
			feed_dict[classifier.input_label]=input_label
			#feed_dict[classifier.input_image]=input_image
			feed_dict[classifier.keep_prob]=1.0

			
			opt, miu, B= sess.run([classifier.test_nll_loss, classifier.r_miu, classifier.B],feed_dict)
			
			#print miu
			marginal_label=np.greater(miu, FLAGS.threshold).astype("int").astype("float")
			#print marginal_label
			#print np.sum(np.abs(input_label-marginal_label))
			#break
			feed_dict[classifier.input_label]=marginal_label
			marginal, miu, B= sess.run([classifier.test_nll_loss, classifier.r_miu, classifier.B],feed_dict)
			
			predictor=Predictor.predictor(True, marginal_label, miu, B)

			optimizer = tf.train.AdamOptimizer(FLAGS.pred_lr)

			train_pred = optimizer.minimize(predictor.nll_loss)

			initialize_uninitialized(sess)
				
			for j in range(100):
	  			sess.run([train_pred], {})
				#print pred_loss

			appro_label=sess.run(predictor.y)
			pred_label=np.greater(appro_label, 0.5).astype("int").astype("float")
			
			for j in range(FLAGS.r_dim):
				for i in range(len(pred_label)):
					if (pred_label[i][j]==1 and input_label[i][j]==1):
						TP+=1
					if (pred_label[i][j]==1 and input_label[i][j]==0):
						FP+=1
					if (pred_label[i][j]==0 and input_label[i][j]==0):
						TN+=1
					if (pred_label[i][j]==0 and input_label[i][j]==1):
						FN+=1
			
			
			
			feed_dict[classifier.input_label]=appro_label
			appro, miu, B= sess.run([classifier.test_nll_loss, classifier.r_miu, classifier.B],feed_dict)
			print "difference ratio", np.mean(np.abs(pred_label-marginal_label))
			
			print "%d/%d"%(k,int( (len(test_idx)-1)/real_batch_size )+1)
			"""

			print "opt nll",opt
			print "marginal nll",marginal
			print "appro nll", appro
			print "\n",
			
			err1=np.sum(np.abs(marginal_label-input_label), axis=1)
			err2=np.sum(np.abs(appro_label-input_label), axis=1)

			error_rate1+=np.sum(np.abs(marginal_label-input_label))/1.0/len(input_label[0])
			error_rate2+=np.sum(np.abs(appro_label-input_label))/1.0/len(input_label[0])			
			wa+=np.sum(np.greater(err2, err1).astype("int"))

			appro=0			
			if (run_cnt>=2000000):
				break"""
			all_nll_loss_opt += opt*(end-start)
			all_nll_loss_marginal += marginal*(end-start)
			all_nll_loss_appro += appro*(end-start)

		nll_loss_opt = all_nll_loss_opt/run_cnt
		nll_loss_marginal = all_nll_loss_marginal/run_cnt
		nll_loss_appro = all_nll_loss_appro/run_cnt

		time_str = datetime.datetime.now().isoformat()

		print "%s\tnll_loss_opt=%.6f\tnll_loss_marginal=%.6f\tnll_loss_appro=%.6f" % (time_str, nll_loss_opt, nll_loss_marginal, nll_loss_appro)	
		return TP, TN, FN, FP
	TP, TN, FN, FP=test_step()
	N=(TP+TN+FN+FP)*1.0
	print "TP=%d, TN=%d, FN=%d, FP=%d"%(TP/N, TN/N, FN/N, FP/N)
	precision=1.0*TP/(TP+FP)
	recall=1.0*TP/(TP+FN) 
	print "Precision", precision
	print "Recall", recall
	print "Accuracy:", 1.0*(TN+TP)/(N)
	print "F1", 2.0*precision*recall/(precision+recall)
	print "F2", (1+4)*precision*recall/(4*precision+recall)

if __name__=='__main__':
	tf.app.run()



