import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class predictor:

	def __init__(self, is_training, marginal_label, miu, B):
		tf.set_random_seed(19950420)
		
		
		self.x = tf.Variable((2*marginal_label-1)*10, dtype=tf.float32, name='predictor/y')
		self.y=tf.sigmoid(self.x)
		self.r_miu = tf.constant(miu, dtype=tf.float32, name='predictor/r_miu')
 		self.B = tf.constant(B, dtype=tf.float32, name='predictor/B')

		############## Sample_r ###############
		self.eps1=tf.constant(1e-6, dtype="float32")

		############## alpha tuning##############
		self.alpha=tf.constant(1.70169,dtype="float32") #logistic(alphax)~cdf_normal(x)

		n_sample=FLAGS.n_train_sample
		if (is_training==False):
			n_sample=FLAGS.n_test_sample
		
		self.noise = tf.random_normal(shape=[n_sample, tf.shape(self.r_miu)[0], tf.shape(self.r_miu)[1]])
		self.sample_r = tf.tensordot(self.noise, self.B, axes=1)+self.r_miu 

		E=tf.sigmoid(self.alpha*self.sample_r)*(1-self.eps1)+self.eps1*0.5 #tensor: n_sample*n_batch*r_dim

		self.E=E
		self.indiv_nll = tf.negative((tf.log(self.E*self.y+(1-self.y)*(1-self.E))), name='predictor/idividual_nll')
		self.logprob=-tf.reduce_sum(self.indiv_nll, axis=2)
		
		self.maxlogprob=tf.reduce_max(self.logprob, axis=0)
		self.Eprob=tf.reduce_mean(tf.exp(self.logprob-self.maxlogprob), axis=0)
		self.nll_loss = tf.reduce_mean(-tf.log(self.Eprob)-self.maxlogprob)

		###### loss ##############
		tf.summary.scalar('predictor/nll_loss',self.nll_loss)


		
