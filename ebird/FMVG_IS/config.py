import tensorflow as tf
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('model_dir','./model/','path to store model')
tf.app.flags.DEFINE_string('summary_dir','./summary','path to store summary_dir')
tf.app.flags.DEFINE_string('config_dir','./config.py','path to config.py')
tf.app.flags.DEFINE_string('checkpoint_path', './model/model-59148','The path to a checkpoint from which to fine-tune.')
tf.app.flags.DEFINE_string('visual_dir','visualization/','path to store visualization codes and data')

tf.app.flags.DEFINE_string('data_dir','../data/loc_100sp_nlcd2011.npy','The path of input .npy data') #loc_100sp_nlcd2011, 1st, fake_sp2_11
tf.app.flags.DEFINE_string('train_idx','../data/train_idx.npy','The path of training data index')
tf.app.flags.DEFINE_string('valid_idx','../data/valid_idx.npy','The path of validation data index')
tf.app.flags.DEFINE_string('test_idx','../data/test_idx.npy','The path of testing data index')

tf.app.flags.DEFINE_integer('batch_size',128,'number of data points in one batch') #128
tf.app.flags.DEFINE_integer('testing_size',128,'the maximal number of data points in testing or validation batch') #128
tf.app.flags.DEFINE_float('learning_rate',0.001,'initial learning rate')
tf.app.flags.DEFINE_float('pred_lr',1,'the learning rate for predictor')
tf.app.flags.DEFINE_integer('max_epoch',200,'max epoch to train')
tf.app.flags.DEFINE_float('weight_decay',0.00001,'weight_decay')
tf.app.flags.DEFINE_float('lr_decay_ratio', 0.5, 'The decay ratio of learning rate')
tf.app.flags.DEFINE_float('lr_decay_times', 4.0, 'How many times does learning rate decay')
tf.app.flags.DEFINE_integer('n_test_sample',10000,'The sampling times for testing')
tf.app.flags.DEFINE_integer('n_train_sample',100,'The sampling times for training') #100

tf.app.flags.DEFINE_integer('r_dim', 100,'r/reallabel dimention') #100
tf.app.flags.DEFINE_integer('r_max_dim', 100,'r_max/max label dimention')
tf.app.flags.DEFINE_integer('nlcd_dim',15,'nlcd dimention')
tf.app.flags.DEFINE_float('save_epoch',2.0,'epochs to save model')
tf.app.flags.DEFINE_integer('max_keep',3,'maximum number of saved model')
tf.app.flags.DEFINE_integer('check_freq',10,'checking frequency')



