import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class FMVG:

	def __init__(self,is_training):

		r_dim = FLAGS.r_dim
		
		self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
		
		self.input_label = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

		self.keep_prob = tf.placeholder(tf.float32)

		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		############## compute mu & sigma ###############


		self.fc_1 = slim.fully_connected(self.input_nlcd, 128, weights_regularizer=weights_regularizer, scope='generator/fc_1')
		self.fc_2 = slim.fully_connected(self.fc_1, 256, weights_regularizer=weights_regularizer, scope='generator/fc_2')
		self.fc_3 = slim.fully_connected(self.fc_2, 256, weights_regularizer=weights_regularizer, scope='generator/fc_3')

		#dropout
		#feature1 = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

		feature1=self.fc_3				
				
		self.r_miu = slim.fully_connected(feature1, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/r_miu')

		self.r_sqrt_sigma=tf.Variable(np.eye(r_dim), dtype=tf.float32, name='generator/r_sqrt_sigma')
		self.sigma=tf.matmul(self.r_sqrt_sigma, tf.transpose(self.r_sqrt_sigma))
		tf.summary.scalar("max-cov", tf.reduce_max(tf.abs(self.sigma)))
		tf.summary.scalar("min-cov", tf.reduce_min(tf.abs(self.sigma)))
		tf.summary.histogram("1D-covariance", tf.reshape(self.sigma,[-1]))
		self.sqrt_diag=1.0/tf.sqrt(tf.diag_part(self.sigma))
		self.correlation=tf.transpose(self.sigma*self.sqrt_diag)*self.sqrt_diag
		tf.summary.histogram("correlation", tf.reshape(self.correlation,[-1]))
		############## Sample_r ###############

		self.eps=tf.constant(1e-6*2.0**(-100), dtype="float64")
		self.alpha=tf.constant((1.0-1e-6),dtype="float32")

		#noise = tf.random_normal(shape=tf.shape(r_miu))
		#sample_r = r_miu + noise
		#self.indiv_nll = tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_r, labels=self.input_label, name='generator/idividual_nll')
		n_sample=FLAGS.n_train_sample
		if (is_training==False):
			n_sample=FLAGS.n_test_sample
		self.noise = tf.random_normal(shape=[n_sample, tf.shape(self.r_miu)[0], tf.shape(self.r_miu)[1]])
		
		self.sample_r = tf.tensordot(self.noise, tf.transpose(self.r_sqrt_sigma), axes=1)+self.r_miu #tensor: n_sample*n_batch*r_dim
		E=tf.sigmoid(tf.cast(self.sample_r, tf.float32))*self.alpha+(1-self.alpha)*0.5

		self.E=E
		self.indiv_nll = tf.negative((tf.log(E)*self.input_label+tf.log(1-E)*(1-self.input_label)), name='generator/idividual_nll')
		self.logprob=-tf.reduce_sum(self.indiv_nll, axis=2)
		
		#########case-1##############
		#self.Eprob=tf.reduce_mean( tf.exp(tf.cast(self.logprob, tf.float32))*self.alpha + self.eps, axis=0)
		#self.nll_loss = tf.reduce_mean(-tf.log(self.Eprob))

		#########case-2##############
		self.maxlogprob=tf.reduce_max(self.logprob, axis=0)
		self.Eprob=tf.reduce_mean(tf.exp(self.logprob-self.maxlogprob), axis=0)
		self.nll_loss = tf.reduce_mean(-tf.log(self.Eprob)-self.maxlogprob)

		#########case-3##############		
		#self.nll_loss = tf.reduce_mean(-self.logprob)


		self.indiv_prob = tf.reduce_mean(E*self.input_label+(1-E)*(1-self.input_label) , axis=0, name='generator/idividual_prob')
		
		

		tf.summary.scalar('nll_loss',self.nll_loss)

		self.l2_loss = tf.cast(tf.add_n(tf.losses.get_regularization_losses()), tf.float32)
		tf.summary.scalar('l2_loss',self.l2_loss)
		
		self.total_loss = self.l2_loss + self.nll_loss
		tf.summary.scalar('total_loss',self.total_loss)

		
