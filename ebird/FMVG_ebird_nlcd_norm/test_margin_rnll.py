import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import FMVG_indiv
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS


def tang_reduce(indiv_prob,label):
	tang=indiv_prob**label*(1.0-indiv_prob)**(1.0-label)
	tang=np.multiply.reduce(tang,1)
	return tang

def main(_):


	print 'reading npy...'

	data = np.load(FLAGS.data_dir)
	test_idx = np.load(FLAGS.test_idx)

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print 'building network...'

	classifier = FMVG_indiv.FMVG(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)
	print 'restoring from '+FLAGS.checkpoint_path


	def test_step():

		print 'Testing...'

		all_margin_loss = 0

		all_indiv_prob = []
		all_label = []
		
		real_batch_size=min(FLAGS.testing_size, len(test_idx))
		for i in range(int(len(test_idx)/real_batch_size)):
			start = real_batch_size*i
			end = min(real_batch_size*(i+1), len(test_idx))

			input_nlcd = get_data.get_nlcd(data,test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])

			feed_dict={}
			feed_dict[classifier.input_nlcd]=input_nlcd
			feed_dict[classifier.input_label]=input_label
			feed_dict[classifier.keep_prob]=1.0


			indiv_prob= sess.run(classifier.indiv_prob,feed_dict)

			for i in indiv_prob:
				all_indiv_prob.append(i)
			for i in input_label:
				all_label.append(i)

		all_indiv_prob = np.array(all_indiv_prob)
		all_label = np.array(all_label)

		all_indiv_prob=np.reshape(all_indiv_prob,(-1,100))
		all_label=np.reshape(all_label,(-1,100))

		return all_indiv_prob,all_label
		# auc = roc_auc_score(all_label,all_indiv_prob)
		# print "new_auc {:g}".format(auc)
	
	all_indiv_prob = 0.0
	all_label = 0.0
	all_fuck = 0.0

	num = FLAGS.n_test_sample

	for i in range(int(num)):
		print i
		indiv_prob,label= test_step()
		#all_indiv_prob.append(indiv_prob)
		all_indiv_prob+=indiv_prob
		all_label=label
		#fuck = np.multiply.reduce(label,1)
		fuck = tang_reduce(indiv_prob,label)
		all_fuck+=fuck

	tang = all_indiv_prob/num
	fuck = all_fuck/num
	epsilon = 1e-100

	loss = all_label * np.log(tang + epsilon) + (1 - all_label) * np.log(1 - tang + epsilon)
	loss = np.sum(loss,axis=1)
	loss = np.mean(loss)
	fuck = np.mean(np.log(fuck))
	
	auc = roc_auc_score(all_label,all_indiv_prob)		
	tang=np.reshape(tang,(-1))
	all_label=np.reshape(all_label,(-1))

	np.save('all_indiv_prob_1wavg.npy',tang)
	np.save('all_label.npy',all_label)

	ap = average_precision_score(all_label,tang)

	time_str = datetime.datetime.now().isoformat()
	new_auc = roc_auc_score(all_label,tang)

	print 'margin results'
	tempstr = "{}: auc {:g}, ap {:g}, recon_loss {:g}, new_auc {:g}".format(time_str, auc, ap, loss, new_auc)
	print(tempstr)

	print 'average log sigma:'+str(fuck)


				

if __name__=='__main__':
	tf.app.run()
