import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import FMVG_test as FMVG
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

def main(_):

	print 'reading npy...'

	data = np.load(FLAGS.data_dir)
	test_idx = np.load(FLAGS.test_idx)

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print 'building network...'

	classifier = FMVG.FMVG(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print 'restoring from '+FLAGS.checkpoint_path


	def test_step():
		print 'Testing...'
		all_nll_loss = 0
		all_l2_loss = 0
		all_total_loss = 0

		all_indiv_prob = []
		all_label = []
		sigma=[]
		real_batch_size=min(FLAGS.testing_size, len(test_idx))
		avg_cor=0
		cnt_cor=0
		
		TP=0
		TN=0
		FN=0
		FP=0
		totNum=0
		for i in range(int( (len(test_idx)-1)/real_batch_size )+1):

			start = real_batch_size*i
			end = min(real_batch_size*(i+1), len(test_idx))

			#input_image= get_data.get_image(images, test_idx[start:end])
			input_nlcd = get_data.get_nlcd(data,test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])

			feed_dict={}
			feed_dict[classifier.input_nlcd]=input_nlcd
			feed_dict[classifier.input_label]=input_label
			#feed_dict[classifier.input_image]=input_image
			feed_dict[classifier.keep_prob]=1.0
			

			sigma, nll_loss, l2_loss, total_loss, indiv_prob, cor, diff, E, B, W, probij, probij_id, miu= sess.run([classifier.sigma, classifier.test_nll_loss, classifier.l2_loss, classifier.total_loss, classifier.indiv_prob,  classifier.cor, classifier.diff, classifier.E, classifier.r_sqrt_sigma, classifier.W, classifier.probij, classifier.probij_id, classifier.r_miu],feed_dict)
			
			pred_label=np.greater(miu, 0).astype(int)
			totNum+=len(miu)
			for j in range(FLAGS.r_dim):
				for i in range(len(pred_label)):
					if (pred_label[i][j]==1 and input_label[i][j]==1):
						TP+=1
					if (pred_label[i][j]==1 and input_label[i][j]==0):
						FP+=1
					if (pred_label[i][j]==0 and input_label[i][j]==0):
						TN+=1
					if (pred_label[i][j]==0 and input_label[i][j]==1):
						FN+=1
			#print "cor:\n",cor
			#print "probij:\n",probij
			#print "probij_id:\n",probij_id
			#print miu
			#print "miu", np.mean(np.sum(miu**2, axis=1))
			avg_cor+=cor
			cnt_cor+=1
	
			all_nll_loss += nll_loss*(end-start)
			all_l2_loss += l2_loss*(end-start)
			all_total_loss += total_loss*(end-start)
	
			for i in indiv_prob:
				all_indiv_prob.append(i)
			for i in input_label:
				all_label.append(i)

		all_indiv_prob = np.array(all_indiv_prob)
		all_label = np.array(all_label)

		auc = roc_auc_score(all_label,all_indiv_prob)

		nll_loss = all_nll_loss/len(test_idx)
		l2_loss = all_l2_loss/len(test_idx)
		total_loss = all_total_loss/len(test_idx)

		all_indiv_prob=np.reshape(all_indiv_prob,(-1))
		all_label=np.reshape(all_label,(-1))
		new_auc = roc_auc_score(all_label,all_indiv_prob)

		ap = average_precision_score(all_label,all_indiv_prob)

		time_str = datetime.datetime.now().isoformat()

		print "%s\tauc=%.6f\tnew_auc=%.6f\tap=%.6f\tnll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % (time_str, auc, new_auc, ap, nll_loss, l2_loss, total_loss)	
		return sigma, avg_cor/(cnt_cor), B, W, TP, TN, FN, FP
	sigma, cor, B, W, TP, TN, FN, FP=test_step()
	
	precision=1.0*TP/(TP+FP)
	recall=1.0*TP/(TP+FN) 
	print "Precision", precision
	print "Recall", recall
	print "Accuracy:", 1.0*(TN+TP)/(TP+TN+FN+FP)
	print "F1", 2.0*precision*recall/(precision+recall)
	print "F2", (1+4)*precision*recall/(4*precision+recall)
	print "Sigma:\n", np.min(sigma), np.mean(sigma), np.max(sigma), "\n", sigma[:5,:5]
	print "Cor:\n", np.min(cor), np.mean(cor), np.max(cor), "\n", cor[:5,:5]
	np.save(FLAGS.visual_dir+"sigma",sigma)
	np.save(FLAGS.visual_dir+"cor",cor)
	np.save(FLAGS.visual_dir+"B",B)
	np.save(FLAGS.visual_dir+"W",np.transpose(W[0]))

if __name__=='__main__':
	tf.app.run()



