import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class FMVG:

	def __init__(self,is_training):
		tf.set_random_seed(19950420)
		r_dim = FLAGS.r_dim
		
		self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
		
		self.input_label = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

		self.keep_prob = tf.placeholder(tf.float32)

		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		############## compute mu & sigma ###############


		self.fc_1 = slim.fully_connected(self.input_nlcd, 128, weights_regularizer=weights_regularizer, scope='generator/fc_1')
		self.fc_2 = slim.fully_connected(self.fc_1, 256, weights_regularizer=weights_regularizer, scope='generator/fc_2')
		self.fc_3 = slim.fully_connected(self.fc_2, 256, weights_regularizer=weights_regularizer, scope='generator/fc_3')

		#dropout
		#feature1 = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

		feature1=self.fc_3				
				
		self.r_miu = slim.fully_connected(feature1, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/r_miu')
		self.W=slim.get_variables_by_name("generator/r_miu/weights")
		
		tf.summary.scalar("miu_abs_mean", tf.reduce_mean(tf.abs(self.r_miu)))

		self.r_sqrt_sigma=tf.Variable(np.eye(r_dim), dtype=tf.float32, name='generator/r_sqrt_sigma')
		self.sigma=tf.matmul(self.r_sqrt_sigma, tf.transpose(self.r_sqrt_sigma))
		tf.summary.scalar("max-cov", tf.reduce_max(tf.abs(self.sigma)))
		tf.summary.scalar("min-cov", tf.reduce_min(self.sigma))
		tf.summary.histogram("1D-covariance", tf.reshape(self.sigma,[-1]))

		self.sqrt_diag=1.0/tf.sqrt(tf.diag_part(self.sigma))
		self.correlation=tf.transpose(self.sigma*self.sqrt_diag)*self.sqrt_diag
		tf.summary.histogram("correlation", tf.reshape(self.correlation,[-1]))
		############## Sample_r ###############

		self.eps2=tf.constant(1e-6*2.0**(-100), dtype="float64")
		self.eps1=tf.constant(1e-6, dtype="float32")
		self.eps3=1e-30
		############## alpha tuning##############
		#self.alpha=tf.Variable(10, dtype=tf.float32, name='hyper/alpha')
		#tf.summary.scalar("alpha", self.alpha)
		self.alpha=tf.constant(1.70169,dtype="float32") #logistic(alphax)~cdf_normal(x)
		self.beta=tf.constant(1,dtype="float32")

		#noise = tf.random_normal(shape=tf.shape(r_miu))
		#sample_r = r_miu + noise
		#self.indiv_nll = tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_r, labels=self.input_label, name='generator/idividual_nll')
		n_sample=FLAGS.n_train_sample
		if (is_training==False):
			n_sample=FLAGS.n_test_sample
		print "n_sample=",n_sample
		self.noise = tf.random_normal(shape=[n_sample, tf.shape(self.r_miu)[0], tf.shape(self.r_miu)[1]])
		
		self.B=tf.transpose(self.r_sqrt_sigma)#*self.sqrt_diag
		#tf.summary.histogram("B", tf.reshape(tf.matmul(tf.transpose(self.B), self.B),[-1]))
		
		self.sample_r = tf.tensordot(self.noise, self.B, axes=1)+self.r_miu #tensor: n_sample*n_batch*r_dim
		
		norm=tf.distributions.Normal(0., 1.)
		E=norm.cdf(self.sample_r)*(1-self.eps1)+self.eps1*0.5
		
		self.E=E
		self.indiv_nll = tf.negative((tf.log(E)*self.input_label+tf.log(1-E)*(1-self.input_label)), name='generator/idividual_nll')
		self.logprob=-tf.reduce_sum(self.indiv_nll, axis=2)
		
		#########case-1##############
		#self.Eprob=tf.reduce_mean( tf.exp(tf.cast(self.logprob, tf.float32))*(1-self.eps1)+ self.eps2, axis=0)
		#self.nll_loss = tf.reduce_mean(-tf.log(self.Eprob))

		#########case-2##############
		self.maxlogprob=tf.reduce_max(self.logprob, axis=0)
		self.Eprob=tf.reduce_mean(tf.exp(self.logprob-self.maxlogprob), axis=0)
		self.test_nll_loss = tf.reduce_mean(-tf.log(self.Eprob)-self.maxlogprob)
		self.nll_loss=self.test_nll_loss
		
		#########case-3##############		
		#self.nll_loss = tf.reduce_mean(-self.logprob)

		######### analysis correlation ##############

		self.probi=tf.reduce_mean(E, axis=[0,1])
		tmp=tf.reduce_mean(E, axis=1)
		tmp2=tf.reshape(self.probi, [tf.shape(self.probi)[0],1])
		self.probij_id=tf.matmul(tmp2, tmp2, False, True)

		self.probij=tf.matmul(tmp, tmp, True, False)/n_sample
		self.probij=self.probij-tf.diag(tf.diag_part(self.probij))+tf.diag(self.probi) #P_ii=P_i
		tmp3=self.probij-self.probij_id
		tmp4=tf.diag(1.0/tf.sqrt(self.probi-self.probi**2)) #1/stderr_i 
		self.cor=tf.matmul(tf.matmul(tmp4,tmp3),tmp4) 
		self.diff=tmp3

		######### analysis stderr, mean, ratio... ##############
		a=tf.cast(self.maxlogprob, dtype="float64")
		b=tf.exp(tf.cast(self.logprob-self.maxlogprob, dtype="float64"))
		realprob=b
		stderr=tf.sqrt(tf.reduce_sum( tf.square(b-tf.reduce_mean(b, axis=0) ), axis=0)/(n_sample-1.0))
		self.std=stderr

		self.maxstderr=tf.reduce_max(stderr)
		self.meanstderr=tf.reduce_mean(stderr)
		self.minstderr=tf.reduce_min(stderr)
		self.meanprob=tf.reduce_mean(realprob, axis=0)
		self.maxprob=tf.exp(a)
		logratio=tf.log(stderr+self.eps3)-tf.log(self.meanprob+self.eps3)

		tf.summary.histogram("logstderr", tf.log(stderr+self.eps3))
		tf.summary.scalar("meanP", tf.reduce_mean(self.meanprob))
		tf.summary.histogram("logratio", logratio)
		
		tf.summary.histogram("realprob", tf.reshape(realprob,[-1]))
		self.meanratio=tf.reduce_mean(logratio)
		self.maxratio=tf.reduce_max(logratio)
		self.minratio=tf.reduce_min(logratio)
		tf.summary.scalar("max-logratio", self.maxratio)
		
		


		self.indiv_prob = tf.reduce_mean(E , axis=0, name='generator/idividual_prob')
		
		
		###### loss ##############
		tf.summary.scalar('nll_loss',self.nll_loss)

		self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())#+FLAGS.weight_decay*tf.nn.l2_loss(self.r_sqrt_sigma)
		tf.summary.scalar('l2_loss',self.l2_loss)
		
		self.total_loss = self.l2_loss + self.nll_loss
		tf.summary.scalar('total_loss',self.total_loss)

		
