import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class FMVG:

	def __init__(self,is_training):
		tf.set_random_seed(19950420)
		r_dim = FLAGS.r_dim
		
		self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
		
		self.input_label = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

		self.keep_prob = tf.placeholder(tf.float32)

		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		############## compute mu & sigma ###############


		self.fc_1 = slim.fully_connected(self.input_nlcd, 128, weights_regularizer=weights_regularizer, scope='generator/fc_1')
		self.fc_2 = slim.fully_connected(self.fc_1, 256, weights_regularizer=weights_regularizer, scope='generator/fc_2')
		self.fc_3 = slim.fully_connected(self.fc_2, 256, weights_regularizer=weights_regularizer, scope='generator/fc_3')

		#dropout
		#feature1 = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

		feature1=self.fc_3				
				
		self.logits = slim.fully_connected(feature1, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/r_miu')
		self.indiv_prob=tf.sigmoid(self.logits)

		self.nll_loss=tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits),1))
		
		
		###### loss ##############
		tf.summary.scalar('nll_loss',self.nll_loss)

		self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())#+FLAGS.weight_decay*tf.nn.l2_loss(self.r_sqrt_sigma)
		tf.summary.scalar('l2_loss',self.l2_loss)
		
		self.total_loss = self.l2_loss + self.nll_loss
		tf.summary.scalar('total_loss',self.total_loss)

		
