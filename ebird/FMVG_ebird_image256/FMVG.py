import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS
class AlexNet(object):
    """Implementation of the AlexNet."""

    def __init__(self, x, keep_prob, num_classes, skip_layer,
                 weights_path='DEFAULT'):
        """Create the graph of the AlexNet model.
        Args:
            x: Placeholder for the input tensor.
            keep_prob: Dropout probability.
            num_classes: Number of classes in the dataset.
            skip_layer: List of names of the layer, that get trained from
                scratch
            weights_path: Complete path to the pretrained weight file, if it
                isn't in the same folder as this code
        """
        # Parse input arguments into class variables
        self.X = x
        self.NUM_CLASSES = num_classes
        self.KEEP_PROB = keep_prob
        self.SKIP_LAYER = skip_layer

        if weights_path == 'DEFAULT':
            self.WEIGHTS_PATH = 'bvlc_alexnet.npy'
        else:
            self.WEIGHTS_PATH = weights_path

        # Call the create function to build the computational graph of AlexNet
        self.create()

    def create(self):
        """Create the network graph."""
        # 1st Layer: Conv (w ReLu) -> Lrn -> Pool
        conv1 = conv(self.X, 11, 11, 96, 4, 4, padding='VALID', name='AlexNet/conv1')
        norm1 = lrn(conv1, 2, 2e-05, 0.75, name='AlexNet/norm1')
        pool1 = max_pool(norm1, 3, 3, 2, 2, padding='VALID', name='AlexNet/pool1')
        
        # 2nd Layer: Conv (w ReLu)  -> Lrn -> Pool with 2 groups
        conv2 = conv(pool1, 5, 5, 256, 1, 1, groups=2, name='AlexNet/conv2')
        norm2 = lrn(conv2, 2, 2e-05, 0.75, name='AlexNet/norm2')
        pool2 = max_pool(norm2, 3, 3, 2, 2, padding='VALID', name='AlexNet/pool2')
        
        # 3rd Layer: Conv (w ReLu)
        conv3 = conv(pool2, 3, 3, 384, 1, 1, name='AlexNet/conv3')

        # 4th Layer: Conv (w ReLu) splitted into two groups
        conv4 = conv(conv3, 3, 3, 384, 1, 1, groups=2, name='AlexNet/conv4')

        # 5th Layer: Conv (w ReLu) -> Pool splitted into two groups
        conv5 = conv(conv4, 3, 3, 256, 1, 1, groups=2, name='AlexNet/conv5')
        pool5 = max_pool(conv5, 3, 3, 2, 2, padding='VALID', name='AlexNet/pool5')

        # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
        flattened = tf.reshape(pool5, [-1, 6*6*256])
        fc6 = fc(flattened, 6*6*256, 4096, name='AlexNet/fc6')
        dropout6 = dropout(fc6, self.KEEP_PROB)

        # 7th Layer: FC (w ReLu) -> Dropout
        fc7 = fc(dropout6, 4096, 4096, name='AlexNet/fc7')
        dropout7 = dropout(fc7, self.KEEP_PROB)

        # 8th Layer: FC and return unscaled activations
        self.fc8 = fc(dropout7, 4096, self.NUM_CLASSES, relu=False, name='AlexNet/fc8')

    def load_initial_weights(self, session):
        """Load weights from file into network.
        As the weights from http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
        come as a dict of lists (e.g. weights['conv1'] is a list) and not as
        dict of dicts (e.g. weights['conv1'] is a dict with keys 'weights' &
        'biases') we need a special load function
        """
        # Load the weights into memory
        weights_dict = np.load(self.WEIGHTS_PATH, encoding='bytes').item()

        # Loop over all layer names stored in the weights dict
        for op_name in weights_dict:

            # Check if layer should be trained from scratch
            if op_name not in self.SKIP_LAYER:

                with tf.variable_scope(op_name, reuse=True):

                    # Assign weights/biases to their corresponding tf variable
                    for data in weights_dict[op_name]:

                        # Biases
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases', trainable=False)
                            session.run(var.assign(data))

                        # Weights
                        else:
                            var = tf.get_variable('weights', trainable=False)
                            session.run(var.assign(data))


def conv(x, filter_height, filter_width, num_filters, stride_y, stride_x, name,
         padding='SAME', groups=1):
    """Create a convolution layer.
    Adapted from: https://github.com/ethereon/caffe-tensorflow
    """
    # Get number of input channels
    input_channels = int(x.get_shape()[-1])

    # Create lambda function for the convolution
    convolve = lambda i, k: tf.nn.conv2d(i, k,
                                         strides=[1, stride_y, stride_x, 1],
                                         padding=padding)

    with tf.variable_scope(name) as scope:
        # Create tf variables for the weights and biases of the conv layer
        weights = tf.get_variable('weights', shape=[filter_height,
                                                    filter_width,
                                                    input_channels/groups,
                                                    num_filters])
        biases = tf.get_variable('biases', shape=[num_filters])

    if groups == 1:
        conv = convolve(x, weights)

    # In the cases of multiple groups, split inputs & weights and
    else:
        # Split input and weights and convolve them separately
        input_groups = tf.split(axis=3, num_or_size_splits=groups, value=x)
        weight_groups = tf.split(axis=3, num_or_size_splits=groups,
                                 value=weights)
        output_groups = [convolve(i, k) for i, k in zip(input_groups, weight_groups)]

        # Concat the convolved output together again
        conv = tf.concat(axis=3, values=output_groups)

    # Add biases
    bias = tf.reshape(tf.nn.bias_add(conv, biases), tf.shape(conv))

    # Apply relu function
    relu = tf.nn.relu(bias, name=scope.name)

    return relu


def fc(x, num_in, num_out, name, relu=True):
    """Create a fully connected layer."""
    with tf.variable_scope(name) as scope:

        # Create tf variables for the weights and biases
        weights = tf.get_variable('weights', shape=[num_in, num_out],
                                  trainable=True)
        biases = tf.get_variable('biases', [num_out], trainable=True)

        # Matrix multiply weights and inputs and add bias
        act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)

    if relu:
        # Apply ReLu non linearity
        relu = tf.nn.relu(act)
        return relu
    else:
        return act


def max_pool(x, filter_height, filter_width, stride_y, stride_x, name,
             padding='SAME'):
    """Create a max pooling layer."""
    return tf.nn.max_pool(x, ksize=[1, filter_height, filter_width, 1],
                          strides=[1, stride_y, stride_x, 1],
                          padding=padding, name=name)


def lrn(x, radius, alpha, beta, name, bias=1.0):
    """Create a local response normalization layer."""
    return tf.nn.local_response_normalization(x, depth_radius=radius,
                                              alpha=alpha, beta=beta,
                                              bias=bias, name=name)


def dropout(x, keep_prob):
    """Create a dropout layer."""
    return tf.nn.dropout(x, keep_prob)
    
class FMVG:

    def __init__(self,is_training):
        tf.set_random_seed(19950420)
        r_dim = FLAGS.r_dim
        
        self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
        
        self.input_label = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

        self.input_image = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.image_dim1, FLAGS.image_dim2, FLAGS.image_dim3],name='input_image')
        
        image_size=FLAGS.image_dim1*FLAGS.image_dim2*FLAGS.image_dim3

        self.keep_prob = tf.placeholder(tf.float32)

        weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

        
        x=AlexNet(x=self.input_image, keep_prob=self.keep_prob, num_classes=512, skip_layer=[]).fc8
        """flatten_hist = tf.reshape(self.input_image,[-1,image_size])

        batch_norm = slim.batch_norm
    
        batch_norm_params = {'is_training':is_training,'updates_collections':tf.GraphKeys.UPDATE_OPS,'decay':0.9,'epsilon':0.00001}
        
        ############## compute mu & sigma ###############
        x = slim.conv2d(scope='encoder/conv1',inputs=self.input_image,num_outputs=16,kernel_size=[11,11],stride=[1,1],
             normalizer_fn=slim.batch_norm, normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)
        
        x = slim.max_pool2d(scope='encoder/pool1',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = slim.conv2d(scope='encoder/conv2',inputs=x,num_outputs=16,kernel_size=[5,5],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

        x = slim.max_pool2d(scope='encoder/pool2',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

        x = slim.conv2d(scope='encoder/conv3',inputs=x,num_outputs=32,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        
    
        x = slim.max_pool2d(scope='encoder/pool3',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = slim.conv2d(scope='encoder/conv4',inputs=x,num_outputs=64,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

        x = slim.max_pool2d(scope='encoder/pool4',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

        x = slim.conv2d(scope='encoder/conv5',inputs=x,num_outputs=32,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        
    
        x = slim.max_pool2d(scope='encoder/pool5',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = tf.reshape(x,[-1,7*7*32])"""

        #x=flatten_hist

        feature2 = tf.concat([self.input_nlcd,x],1)

        #x = feature2 #tf.reshape(self.input_image,[-1,image_size])
        
            
        self.fc_1 = slim.fully_connected(x, 512, weights_regularizer=weights_regularizer, scope='generator/fc_1')
        self.fc_2 = slim.fully_connected(self.fc_1, 1024, weights_regularizer=weights_regularizer, scope='generator/fc_2')
        self.fc_3 = slim.fully_connected(self.fc_2, 512, weights_regularizer=weights_regularizer, scope='generator/fc_3')
        
        #dropout
        x = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

        feature1=x            
                
        self.r_miu = slim.fully_connected(feature1, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/r_miu')
        tf.summary.scalar("miu_abs_mean", tf.reduce_mean(tf.abs(self.r_miu)))

        self.r_sqrt_sigma=tf.Variable(np.eye(r_dim), dtype=tf.float32, name='generator/r_sqrt_sigma')
        self.sigma=tf.matmul(self.r_sqrt_sigma, tf.transpose(self.r_sqrt_sigma))
        tf.summary.scalar("max-cov", tf.reduce_max(tf.abs(self.sigma)))
        tf.summary.scalar("min-cov", tf.reduce_min(self.sigma))
        tf.summary.histogram("1D-covariance", tf.reshape(self.sigma,[-1]))

        self.sqrt_diag=1.0/tf.sqrt(tf.diag_part(self.sigma))
        self.correlation=tf.transpose(self.sigma*self.sqrt_diag)*self.sqrt_diag
        tf.summary.histogram("correlation", tf.reshape(self.correlation,[-1]))
        ############## Sample_r ###############

        self.eps2=tf.constant(1e-6*2.0**(-100), dtype="float64")
        self.eps1=tf.constant(1e-6, dtype="float32")
        self.eps3=1e-30
        ############## alpha tuning##############
        #self.alpha=tf.Variable(10, dtype=tf.float32, name='hyper/alpha')
        #tf.summary.scalar("alpha", self.alpha)
        self.alpha=tf.constant(1.70169,dtype="float32") #logistic(alphax)~cdf_normal(x)
        self.beta=tf.constant(1,dtype="float32")

        #noise = tf.random_normal(shape=tf.shape(r_miu))
        #sample_r = r_miu + noise
        #self.indiv_nll = tf.nn.sigmoid_cross_entropy_with_logits(logits=sample_r, labels=self.input_label, name='generator/idividual_nll')
        n_sample=FLAGS.n_train_sample
        if (is_training==False):
            n_sample=FLAGS.n_test_sample
        print "n_sample=",n_sample
        self.noise = tf.random_normal(shape=[n_sample, tf.shape(self.r_miu)[0], tf.shape(self.r_miu)[1]])
        
        self.B=tf.transpose(self.r_sqrt_sigma)#*self.sqrt_diag
        #tf.summary.histogram("B", tf.reshape(tf.matmul(tf.transpose(self.B), self.B),[-1]))
        
        self.sample_r = tf.tensordot(self.noise, self.B, axes=1)+self.r_miu #tensor: n_sample*n_batch*r_dim
        E=tf.sigmoid(self.alpha*self.sample_r)*(1-self.eps1)+self.eps1*0.5

        self.E=E
        self.indiv_nll = tf.negative((tf.log(E)*self.input_label+tf.log(1-E)*(1-self.input_label)), name='generator/idividual_nll')
        self.logprob=-tf.reduce_sum(self.indiv_nll, axis=2)
        
        #########case-1##############
        #self.Eprob=tf.reduce_mean( tf.exp(tf.cast(self.logprob, tf.float32))*(1-self.eps1)+ self.eps2, axis=0)
        #self.nll_loss = tf.reduce_mean(-tf.log(self.Eprob))

        #########case-2##############
        self.maxlogprob=tf.reduce_max(self.logprob, axis=0)
        self.Eprob=tf.reduce_mean(tf.exp(self.logprob-self.maxlogprob), axis=0)
        self.test_nll_loss = tf.reduce_mean(-tf.log(self.Eprob)-self.maxlogprob)
        self.nll_loss=self.test_nll_loss
        
        #########case-3##############        
        #self.nll_loss = tf.reduce_mean(-self.logprob)

        ######### analysis correlation ##############
        
        ######### analysis stderr, mean, ratio... ##############
        a=tf.cast(self.maxlogprob, dtype="float64")
        b=tf.exp(tf.cast(self.logprob-self.maxlogprob, dtype="float64"))
        realprob=b
        stderr=tf.sqrt(tf.reduce_sum( tf.square(b-tf.reduce_mean(b, axis=0) ), axis=0)/(n_sample-1.0))
        self.std=stderr

        self.maxstderr=tf.reduce_max(stderr)
        self.meanstderr=tf.reduce_mean(stderr)
        self.minstderr=tf.reduce_min(stderr)
        self.meanprob=tf.reduce_mean(realprob, axis=0)
        self.maxprob=tf.exp(a)
        logratio=tf.log(stderr+self.eps3)-tf.log(self.meanprob+self.eps3)

        tf.summary.histogram("logstderr", tf.log(stderr+self.eps3))
        tf.summary.scalar("meanP", tf.reduce_mean(self.meanprob))
        tf.summary.histogram("logratio", logratio)
        
        tf.summary.histogram("realprob", tf.reshape(realprob,[-1]))
        self.meanratio=tf.reduce_mean(logratio)
        self.maxratio=tf.reduce_max(logratio)
        self.minratio=tf.reduce_min(logratio)
        tf.summary.scalar("max-logratio", self.maxratio)
        
        


        self.indiv_prob = tf.reduce_mean(E*self.input_label+(1-E)*(1-self.input_label) , axis=0, name='generator/idividual_prob')
        
        
        ###### loss ##############
        tf.summary.scalar('nll_loss',self.nll_loss)

        self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())#+FLAGS.weight_decay*tf.nn.l2_loss(self.r_sqrt_sigma)
        tf.summary.scalar('l2_loss',self.l2_loss)
        
        self.total_loss = self.l2_loss + self.nll_loss
        tf.summary.scalar('total_loss',self.total_loss)

        
