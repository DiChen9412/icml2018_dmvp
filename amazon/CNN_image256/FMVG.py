import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class FMVG:

    def __init__(self,is_training):
        tf.set_random_seed(19950420)
        r_dim = FLAGS.r_dim
        
        #self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
        
        self.input_label = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

        self.input_image = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.image_dim1, FLAGS.image_dim2, FLAGS.image_dim3],name='input_image')
        
        image_size=FLAGS.image_dim1*FLAGS.image_dim2*FLAGS.image_dim3

        self.keep_prob = tf.placeholder(tf.float32)

        weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

        flatten_hist = tf.reshape(self.input_image,[-1,image_size])

        batch_norm = slim.batch_norm
    
        batch_norm_params = {'is_training':is_training,'updates_collections':tf.GraphKeys.UPDATE_OPS,'decay':0.9,'epsilon':0.00001}
        
        ############## compute mu & sigma ###############
        
        x = slim.conv2d(scope='encoder/conv1',inputs=self.input_image,num_outputs=16,kernel_size=[11,11],stride=[1,1],
             normalizer_fn=slim.batch_norm, normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)
        
        x = slim.max_pool2d(scope='encoder/pool1',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = slim.conv2d(scope='encoder/conv2',inputs=x,num_outputs=16,kernel_size=[5,5],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

        x = slim.max_pool2d(scope='encoder/pool2',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

        x = slim.conv2d(scope='encoder/conv3',inputs=x,num_outputs=32,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        
    
        x = slim.max_pool2d(scope='encoder/pool3',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = slim.conv2d(scope='encoder/conv4',inputs=x,num_outputs=64,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

        x = slim.max_pool2d(scope='encoder/pool4',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

        x = slim.conv2d(scope='encoder/conv5',inputs=x,num_outputs=32,kernel_size=[3,3],stride=[1,1],
            normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        
    
        x = slim.max_pool2d(scope='encoder/pool5',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
        
        x = tf.reshape(x,[-1,8*8*32])

        #x=flatten_hist

        feature2 = x

        #x = feature2 #tf.reshape(self.input_image,[-1,image_size])
        
            
        self.fc_1 = slim.fully_connected(x, 256, weights_regularizer=weights_regularizer, scope='generator/fc_1')
        #x=self.fc_1
        x = slim.dropout(self.fc_1, keep_prob=self.keep_prob, is_training=is_training)
        
        self.fc_2 = slim.fully_connected(self.fc_1, 128, weights_regularizer=weights_regularizer, scope='generator/fc_2')
        x = slim.dropout(self.fc_2, keep_prob=self.keep_prob, is_training=is_training)
        
        #x=self.fc_2
        #self.fc_3 = slim.fully_connected(x, 64, weights_regularizer=weights_regularizer, scope='generator/fc_3')
        
        #dropout
        #x = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

        feature1=x    
                
        
        self.logits = slim.fully_connected(feature1, FLAGS.r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='decoder/logits')
        
        self.eps1=tf.constant(1e-6, dtype="float32")
        self.output = tf.sigmoid(self.logits)*(1-1e-6)+0.5*1e-6
        
        self.indiv_prob=self.output
        
        self.nll_loss=tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits),1))
        #=tf.reduce_mean(tf.reduce_sum(-tf.log(self.pred_prob),1))
        
        
        
        ###### loss ##############
        tf.summary.scalar('nll_loss',self.nll_loss)

        self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())#+FLAGS.weight_decay*tf.nn.l2_loss(self.r_sqrt_sigma)
        tf.summary.scalar('l2_loss',self.l2_loss)
        
        self.total_loss = self.l2_loss + self.nll_loss
        tf.summary.scalar('total_loss',self.total_loss)

        
