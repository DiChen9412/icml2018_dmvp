import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import FMVG
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

configFile=open(FLAGS.config_dir, "r")
content=[line.strip("\n") for line in configFile]
configFile.close()
for i in range(len(content)):
	if ("checkpoint_path" in content[i]):
		content[i]="tf.app.flags.DEFINE_string('checkpoint_path', './model/model-%d','The path to a checkpoint from which to fine-tune.')"%51516
configFile=open(FLAGS.config_dir, "w")
for line in content:
	configFile.write(line+"\n")
configFile.close()
