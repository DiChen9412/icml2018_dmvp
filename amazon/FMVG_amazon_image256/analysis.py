import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import FMVG_analysis as FMVG
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

def MakeSummary(name, value):
	"""Creates a tf.Summary proto with the given name and value."""
	summary = tf.Summary()
	val = summary.value.add()
	val.tag = str(name)
	val.simple_value = float(value)
	return summary

#def train_step(input_nlcd, input_label, smooth_ce_loss, smooth_l2_loss,smooth_total_loss):
def train_step(sess, hg, merged_summary, summary_writer, input_label, input_nlcd, train_op, global_step):

	feed_dict={}
	feed_dict[hg.input_nlcd]=input_nlcd
	feed_dict[hg.input_label]=input_label
	feed_dict[hg.keep_prob]=0.8

	#train_op,
	step,nll_loss,l2_loss,total_loss, summary, indiv_prob= sess.run([global_step, hg.nll_loss, hg.l2_loss, hg.total_loss, merged_summary, hg.indiv_prob], feed_dict)
	#Eprob, E, indiv_nll, logprob, sigma, r_sqrt_sigma, r_miu, noise, sample_r=sess.run([hg.Eprob, hg.E, hg.indiv_nll, hg.logprob, hg.sigma, hg.r_sqrt_sigma, hg.r_miu, hg.noise, hg.sample_r], feed_dict)
	maxstderr, meanstderr, minstderr, maxratio, meanratio, minratio, meanprob, std, maxprob, noise, E=sess.run([tf.log(hg.maxstderr), tf.log(hg.meanstderr),tf.log(hg.minstderr), hg.maxratio, hg.meanratio, hg.minratio, hg.meanprob, hg.std, hg.maxprob, hg.noise, hg.E], feed_dict)
	time_str = datetime.datetime.now().isoformat()
	summary_writer.add_summary(summary,step)

	#print Eprob[:10]
	#print E[0][0][:10]
	#print input_label[0][:10]
	#print indiv_nll[0][0][:10]
	#print logprob
	#print "log maxstderr", maxstderr
	#print "log meanstderr", meanstderr
	#print "log minstderr", minstderr
	
	#print "log meanratio", meanratio
	#print "log minratio", minratio
	#print "log meanprob", meanprob
	
	print "log maxratio", maxratio
	maxn=0
	pos=-1
	for i in range(len(std)):
		if (std[i]/meanprob[i]>maxn):
			maxn=std[i]/meanprob[i]
			pos=i;
	print "max-ratio", maxn, "pos", pos	
	print "std:\n",std[pos]
	print "meanprob\n", meanprob[pos]
	print "maxprob\n", maxprob[pos]
	print "realprob\n",meanprob[pos]*maxprob[pos]
	#print "noise\n",noise[0][0][:5]
	#print "hg.input_nlcd",input_nlcd[pos]
	#print "hg.input_nlcd",input_label[pos]

	"""print "sigma:\n", sigma
	print "sqrt_sigma:\n", r_sqrt_sigma
	print "miu:\n", r_miu
	print "noise:\n", noise
	print "sample_r:\n", sample_r
	print "E:\n", E"""
	return indiv_prob, nll_loss, l2_loss, total_loss, meanprob*maxprob


def main(_):

	print 'reading npy...'
	np.random.seed(19950420)
	data = np.load(FLAGS.data_dir)
	train_idx = np.load(FLAGS.train_idx)
	valid_idx = np.load(FLAGS.valid_idx)

	one_epoch_iter = len(train_idx)/FLAGS.batch_size

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)
	#tf.set_random_seed(19950420)
	print 'showing the parameters...\n'

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	for (key, value) in FLAGS.__dict__['__flags'].items():
		print "%s	%s"%(key, value)
	print "\n"


	print 'building network...'

	
	hg = FMVG.FMVG(is_training=True)

	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = tf.train.exponential_decay(FLAGS.learning_rate, global_step, (1.0/FLAGS.lr_decay_times)*(FLAGS.max_epoch*one_epoch_iter), FLAGS.lr_decay_ratio, staircase=True)

	tf.summary.scalar('learning_rate', learning_rate)

	optimizer = tf.train.AdamOptimizer(learning_rate)

	update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
	with tf.control_dependencies(update_ops):
		train_op = optimizer.minimize(hg.total_loss, global_step = global_step)

	merged_summary = tf.summary.merge_all() # gather all summary nodes together
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph)

	sess.run(tf.global_variables_initializer())
	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)
	print 'building finished'

	best_loss = 1e10
	best_iter = 0
	smooth_nll_loss=0.0
	smooth_l2_loss=0.0
	smooth_total_loss=0.0
	temp_label=[]	
	temp_indiv_prob=[]
	
	mean=0
	current_step=0
	for one_epoch in range(100000):
		
		print('epoch '+str(one_epoch+1)+' starts!')

		tmp_mean=0
		n=1000
		for i in range(n):
			idx=3
			start = idx*FLAGS.batch_size
			end = (idx+1)*FLAGS.batch_size

			input_nlcd = get_data.get_nlcd(data,train_idx[start:end])
			input_label = get_data.get_label(data,train_idx[start:end])

			indiv_prob, nll_loss, l2_loss, total_loss, Eprob= train_step(sess, hg, merged_summary, summary_writer, input_label,input_nlcd, train_op, global_step)
			current_step+=1
			summary_writer.add_summary(MakeSummary('log Eprob',np.log(Eprob[0])),current_step)
			tmp_mean+=Eprob
		tmp_mean/=n
		print "current mean=", tmp_mean[:5]
		summary_writer.add_summary(MakeSummary('log tmp_mean',np.log(tmp_mean[0])),current_step)
		mean+=tmp_mean
		print "global mean=", mean[:5]/(one_epoch+1)
		summary_writer.add_summary(MakeSummary('log global_mean',np.log(mean[0]/(one_epoch+1))),current_step)
	


	print 'training completed !'
	print 'the best loss on validation is '+str(best_loss)
	print 'the best checkpoint is '+str(best_iter)
	

if __name__=='__main__':
	tf.app.run()
