import tensorflow as tf
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('model_dir','./model/','path to store model')
tf.app.flags.DEFINE_string('summary_dir','./summary','path to store summary_dir')
tf.app.flags.DEFINE_string('config_dir','./config.py','path to config.py')
tf.app.flags.DEFINE_string('checkpoint_path', './model/model-8127','The path to a checkpoint from which to fine-tune.')
tf.app.flags.DEFINE_string('visual_dir','visualization/','path to store visualization codes and data')

tf.app.flags.DEFINE_string('data_dir','../data/amazon_labels.npy','The path of input .npy data')
tf.app.flags.DEFINE_string('image_dir','../data/amazon_images/','The path of input image data')
tf.app.flags.DEFINE_string('train_idx','../data/train_idx.npy','The path of training data index')
tf.app.flags.DEFINE_string('valid_idx','../data/valid_idx.npy','The path of validation data index')
tf.app.flags.DEFINE_string('test_idx','../data/test_idx.npy','The path of testing data index')

tf.app.flags.DEFINE_integer('batch_size',128,'number of data points in one batch') #128
tf.app.flags.DEFINE_integer('testing_size',128,'the maximal number of data points in testing or validation batch') #128
tf.app.flags.DEFINE_float('learning_rate',0.001,'initial learning rate')
tf.app.flags.DEFINE_float('min_learning_rate',0.00003,'minimum learning rate')
tf.app.flags.DEFINE_float('pred_lr',1,'the learning rate for predictor')
tf.app.flags.DEFINE_integer('max_epoch',200,'max epoch to train')
tf.app.flags.DEFINE_float('weight_decay',0.00001,'weight_decay')
tf.app.flags.DEFINE_float('lr_decay_ratio', 0.5, 'The decay ratio of learning rate')
tf.app.flags.DEFINE_float('lr_decay_times', 10, 'How many times does learning rate decay')
tf.app.flags.DEFINE_float('dropout_rate', 0.5, 'The dropout ratio')
tf.app.flags.DEFINE_integer('n_test_sample',10000,'The sampling times for testing')
tf.app.flags.DEFINE_integer('n_train_sample',1000,'The sampling times for training') #100

tf.app.flags.DEFINE_integer('r_dim', 17,'r/reallabel dimention') #100
tf.app.flags.DEFINE_integer('image_dim1', 256,'r/image dimention 1') 
tf.app.flags.DEFINE_integer('image_dim2', 256,'r/image dimention 2') 
tf.app.flags.DEFINE_integer('image_dim3', 3,'r/image dimention 3') 
tf.app.flags.DEFINE_integer('r_max_dim', 17,'r_max/max label dimention')

tf.app.flags.DEFINE_float('save_epoch',1.0,'epochs to save model')
tf.app.flags.DEFINE_integer('max_keep',3,'maximum number of saved model')
tf.app.flags.DEFINE_integer('check_freq',10,'checking frequency')



